#/*** Author: Sharad Gupta ****/
from bs4 import BeautifulSoup
from gi.repository import Notify
import urllib ,schedule , time
def job():
	r = urllib.urlopen('https://www.dbs.com.sg/personal/rates-online/foreign-currency-foreign-exchange.page').read()
	soup = BeautifulSoup(r,'html.parser')	
	row = soup.find('tr', attrs={'class':'odd filter_currency filter_Indian_Rupee'})
	inverserate = float(row.find('td', attrs={'class':'column-3'}).text)
	actualrate = 100.0/inverserate
	Notify.init("Exchange Rate Fetcher")
	Notify.Notification.new("DBS Exchange Rate(Sgd -> Inr)", str(actualrate)).show()

schedule.every(10).minutes.do(job)
while True:
	schedule.run_pending()
	time.sleep(1)


